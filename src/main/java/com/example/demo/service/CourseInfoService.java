package com.example.demo.service;


import com.example.demo.mapper.CourseInfoDynamicSqlSupport;
import com.example.demo.mapper.CourseInfoMapper;
import com.example.demo.model.CourseInfo;
import org.mybatis.dynamic.sql.render.RenderingStrategies;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.mybatis.dynamic.sql.SqlBuilder.*;

@Service
public class CourseInfoService {

    private CourseInfoMapper courseInfoMapper;

    @Autowired
    public void setCourseInfoMapper(CourseInfoMapper courseInfoMapper) {
        this.courseInfoMapper = courseInfoMapper;
    }

    public CourseInfo getInfo(String courseInfoID){
        SelectStatementProvider selectStatementProvider = select(CourseInfoMapper.selectList)
                .from(CourseInfoDynamicSqlSupport.courseInfo)
                .where(CourseInfoDynamicSqlSupport.id,isEqualTo(courseInfoID))
                .build().render(RenderingStrategies.MYBATIS3);
        return courseInfoMapper.selectMany(selectStatementProvider).get(0);
    }
}
