package com.example.demo.service;


import com.example.demo.common.status.CourseInfoUserStatus;
import com.example.demo.common.util.DateUtils;
import com.example.demo.common.util.IdUtils;
import com.example.demo.common.util.StringUtils;
import com.example.demo.mapper.*;
import com.example.demo.model.CourseInfo;
import com.example.demo.model.CourseInfoUser;
import com.example.demo.view.vo.CourseDetailsVo;
import org.mybatis.dynamic.sql.render.RenderingStrategies;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.mybatis.dynamic.sql.SqlBuilder.*;

@Service
public class CourseInfoUserService {

    private CourseInfoUserMapper courseInfoUserMapper;

    private CourseInfoMapper courseInfoMapper;

    @Autowired
    public void setCourseInfoMapper(CourseInfoMapper courseInfoMapper) {
        this.courseInfoMapper = courseInfoMapper;
    }

    @Autowired
    public void setCourseInfoUserMapper(CourseInfoUserMapper courseInfoUserMapper) {
        this.courseInfoUserMapper = courseInfoUserMapper;
    }

    public String getRateOfCourse(String userID, String courseID) {
        SelectStatementProvider selectStatementProvider = select(count())
                .from(CourseInfoUserDynamicSqlSupport.courseInfoUser)
                .leftJoin(CourseInfoDynamicSqlSupport.courseInfo)
                .on(CourseInfoDynamicSqlSupport.id, equalTo(CourseInfoUserDynamicSqlSupport.courseInfoId))
                .leftJoin(CourseDynamicSqlSupport.course)
                .on(CourseDynamicSqlSupport.id, equalTo(CourseInfoDynamicSqlSupport.courseId))
                .where(CourseInfoUserDynamicSqlSupport.state, isEqualTo(CourseInfoUserStatus.DISABLE.getCode()))
                .and(CourseDynamicSqlSupport.id, isEqualTo(courseID))
                .and(CourseInfoUserDynamicSqlSupport.userId, isEqualTo(userID))
                .build().render(RenderingStrategies.MYBATIS3);
        long disable = courseInfoUserMapper.count(selectStatementProvider);
        SelectStatementProvider selectStatementProvider1 = select(count())
                .from(CourseInfoUserDynamicSqlSupport.courseInfoUser)
                .leftJoin(CourseInfoDynamicSqlSupport.courseInfo)
                .on(CourseInfoDynamicSqlSupport.id, equalTo(CourseInfoUserDynamicSqlSupport.courseInfoId))
                .leftJoin(CourseDynamicSqlSupport.course)
                .on(CourseDynamicSqlSupport.id, equalTo(CourseInfoDynamicSqlSupport.courseId))
                .where(CourseDynamicSqlSupport.id, isEqualTo(courseID))
                .and(CourseInfoUserDynamicSqlSupport.userId, isEqualTo(userID))
                .build().render(RenderingStrategies.MYBATIS3);
        long total = courseInfoUserMapper.count(selectStatementProvider1);
        return String.format("%.0f", ((double) (total - disable) / (double) total) * 100) + "%";
    }

    public List<CourseDetailsVo> getCourseDetails(String userID, String courseID) {
        SelectStatementProvider selectStatementProvider = select(CourseInfoMapper.selectList)
                .from(CourseInfoDynamicSqlSupport.courseInfo)
                .leftJoin(CourseInfoUserDynamicSqlSupport.courseInfoUser)
                .on(CourseInfoDynamicSqlSupport.id, equalTo(CourseInfoUserDynamicSqlSupport.courseInfoId))
                .where(CourseInfoUserDynamicSqlSupport.userId, isEqualTo(userID))
                .and(CourseInfoDynamicSqlSupport.courseId, isEqualTo(courseID))
                .orderBy(CourseInfoDynamicSqlSupport.courseHour)
                .build().render(RenderingStrategies.MYBATIS3);
        List<CourseDetailsVo> courseDetailsVos = new ArrayList<>();
        for (CourseInfo courseInfo : courseInfoMapper.selectMany(selectStatementProvider)) {
            CourseDetailsVo courseDetailsVo = new CourseDetailsVo();
            courseDetailsVo.setId(courseInfo.getId());
            courseDetailsVo.setUrl(courseInfo.getCourseUrl());
            courseDetailsVo.setName(courseInfo.getCourseName());
            courseDetailsVo.setState(findState(userID,courseID,courseInfo.getId()));
            courseDetailsVos.add(courseDetailsVo);
        }
        return courseDetailsVos;
    }

    public String findState(String userID,String courseID,String courseInfoId){
        SelectStatementProvider selectStatementProvider = select(CourseInfoUserMapper.selectList)
                .from(CourseInfoUserDynamicSqlSupport.courseInfoUser)
                .leftJoin(CourseInfoDynamicSqlSupport.courseInfo)
                .on(CourseInfoDynamicSqlSupport.id, equalTo(CourseInfoUserDynamicSqlSupport.courseInfoId))
                .where(CourseInfoUserDynamicSqlSupport.userId, isEqualTo(userID))
                .and(CourseInfoDynamicSqlSupport.courseId, isEqualTo(courseID))
                .and(CourseInfoDynamicSqlSupport.id, isEqualTo(courseInfoId))
                .build().render(RenderingStrategies.MYBATIS3);
        if (courseInfoUserMapper.selectOne(selectStatementProvider).get().getState().equals(CourseInfoUserStatus.DONE.getCode())) {
            return CourseInfoUserStatus.DONE.getMessage();
        } else if (courseInfoUserMapper.selectOne(selectStatementProvider).get().getState().equals(CourseInfoUserStatus.OK.getCode())) {
            return CourseInfoUserStatus.OK.getMessage();
        } else {
            return CourseInfoUserStatus.DISABLE.getMessage();
        }
    }

    public void Join(String courseID,String userID){
        SelectStatementProvider selectStatementProvider = select(CourseInfoMapper.selectList)
                .from(CourseInfoDynamicSqlSupport.courseInfo)
                .where(CourseInfoDynamicSqlSupport.courseId,isEqualTo(courseID))
                .orderBy(CourseInfoDynamicSqlSupport.courseHour)
                .build().render(RenderingStrategies.MYBATIS3);
        for(CourseInfo courseInfo : courseInfoMapper.selectMany(selectStatementProvider)){
            CourseInfoUser courseInfoUser = new CourseInfoUser();
            courseInfoUser.setCourseInfoId(courseInfo.getId());
            courseInfoUser.setUserId(userID);
            courseInfoUser.setId(IdUtils.randomUUID());
            courseInfoUser.setState(CourseInfoUserStatus.DISABLE.getCode());
            courseInfoUser.setCreateTime(DateUtils.getNowDate());
            courseInfoUser.setUpdateTime(DateUtils.getNowDate());
            courseInfoUserMapper.insertSelective(courseInfoUser);
        }
    }

    public List<String> getNote(String courseInfoID ,String userID){
        List<String> note = new ArrayList<>();
        if(userID .equals("jjjjjjj") ){
            SelectStatementProvider selectStatementProvider = select(CourseInfoUserMapper.selectList)
                    .from(CourseInfoUserDynamicSqlSupport.courseInfoUser)
                    .where(CourseInfoUserDynamicSqlSupport.courseInfoId, isEqualTo(courseInfoID))
                    .and(CourseInfoUserDynamicSqlSupport.note,isNotNull())
                    .orderBy(CourseInfoUserDynamicSqlSupport.updateTime)
                    .build().render(RenderingStrategies.MYBATIS3);
            for(CourseInfoUser courseInfoUser : courseInfoUserMapper.selectMany(selectStatementProvider)){
                note.add(courseInfoUser.getNote());
            }
        }else {
            SelectStatementProvider selectStatementProvider = select(CourseInfoUserMapper.selectList)
                    .from(CourseInfoUserDynamicSqlSupport.courseInfoUser)
                    .where(CourseInfoUserDynamicSqlSupport.courseInfoId, isEqualTo(courseInfoID))
                    .and(CourseInfoUserDynamicSqlSupport.userId, isEqualTo(userID))
                    .build().render(RenderingStrategies.MYBATIS3);
            CourseInfoUser courseInfoUser = courseInfoUserMapper.selectMany(selectStatementProvider).get(0);
            courseInfoUser.setState(CourseInfoUserStatus.OK.getCode());
            courseInfoUser.setUpdateTime(DateUtils.getNowDate());
            courseInfoUserMapper.updateByPrimaryKeySelective(courseInfoUser);
            if(!StringUtils.isEmpty(courseInfoUser.getNote())){
                note.add(courseInfoUser.getNote());
            }else {
                note.add("暂无笔记，快去添加笔记吧！");
            }
        }
        return note;
    }

    public void noteCourseInfo(String userId, String courseInfoID, String note) {
        SelectStatementProvider selectStatementProvider = select(CourseInfoUserMapper.selectList)
                .from(CourseInfoUserDynamicSqlSupport.courseInfoUser)
                .where(CourseInfoUserDynamicSqlSupport.userId,isEqualTo(userId))
                .and(CourseInfoUserDynamicSqlSupport.courseInfoId,isEqualTo(courseInfoID))
                .build().render(RenderingStrategies.MYBATIS3);
        CourseInfoUser courseInfoUser = courseInfoUserMapper.selectMany(selectStatementProvider).get(0);
        courseInfoUser.setUpdateTime(DateUtils.getNowDate());
        courseInfoUser.setNote(note);
    }
}
