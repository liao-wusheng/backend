package com.example.demo.service;


import com.example.demo.common.util.DateUtils;
import com.example.demo.common.util.IdUtils;
import com.example.demo.mapper.CourseUserDynamicSqlSupport;
import com.example.demo.mapper.CourseUserMapper;
import com.example.demo.model.CourseUser;
import org.mybatis.dynamic.sql.render.RenderingStrategies;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.mybatis.dynamic.sql.SqlBuilder.*;

@Service
public class CourseUserService {

    private CourseUserMapper courseUserMapper;

    @Autowired
    public void setCourseUserMapper(CourseUserMapper courseUserMapper) {
        this.courseUserMapper = courseUserMapper;
    }

    public List<String> getCourseEvaluate(String courseID){
        SelectStatementProvider selectStatementProvider = select(CourseUserMapper.selectList)
                .from(CourseUserDynamicSqlSupport.courseUser)
                .where(CourseUserDynamicSqlSupport.courseId,isEqualTo(courseID))
                .and(CourseUserDynamicSqlSupport.evaluate,isNotNull())
                .orderBy(CourseUserDynamicSqlSupport.updateTime)
                .build().render(RenderingStrategies.MYBATIS3);
        List<String> strings = new ArrayList<>();
        for(CourseUser courseUser : courseUserMapper.selectMany(selectStatementProvider)){
            strings.add(courseUser.getEvaluate());
        }
        return strings;
    }

    public List<String> getCourseNote(String courseID){
        SelectStatementProvider selectStatementProvider = select(CourseUserMapper.selectList)
                .from(CourseUserDynamicSqlSupport.courseUser)
                .where(CourseUserDynamicSqlSupport.courseId,isEqualTo(courseID))
                .and(CourseUserDynamicSqlSupport.note,isNotNull())
                .orderBy(CourseUserDynamicSqlSupport.updateTime)
                .build().render(RenderingStrategies.MYBATIS3);
        List<String> strings = new ArrayList<>();
        for(CourseUser courseUser : courseUserMapper.selectMany(selectStatementProvider)){
            strings.add(courseUser.getNote());
        }
        return strings;
    }

    public boolean IsJoin(String courseID,String userID){
        SelectStatementProvider selectStatementProvider = select(CourseUserMapper.selectList)
                .from(CourseUserDynamicSqlSupport.courseUser)
                .where(CourseUserDynamicSqlSupport.courseId,isEqualTo(courseID))
                .and(CourseUserDynamicSqlSupport.userId,isEqualTo(userID))
                .build().render(RenderingStrategies.MYBATIS3);
        return courseUserMapper.selectMany(selectStatementProvider).size() != 0;
    }

    public void Join(String courseID,String userID){
        CourseUser courseUser = new CourseUser();
        courseUser.setId(IdUtils.randomUUID());
        courseUser.setCourseId(courseID);
        courseUser.setUserId(userID);
        courseUser.setCreateTime(DateUtils.getNowDate());
        courseUser.setUpdateTime(DateUtils.getNowDate());
        courseUserMapper.insertSelective(courseUser);
    }

    public void evaluateCourse(String userId, String courseID,String evaluate) {
        SelectStatementProvider selectStatementProvider = select(CourseUserMapper.selectList)
                .from(CourseUserDynamicSqlSupport.courseUser)
                .where(CourseUserDynamicSqlSupport.userId,isEqualTo(userId))
                .and(CourseUserDynamicSqlSupport.courseId,isEqualTo(courseID))
                .build().render(RenderingStrategies.MYBATIS3);
        CourseUser courseUser = courseUserMapper.selectMany(selectStatementProvider).get(0);
        courseUser.setUpdateTime(DateUtils.getNowDate());
        courseUser.setEvaluate(evaluate);
        courseUserMapper.updateByPrimaryKey(courseUser);
    }

    public void noteCourse(String userId, String courseID, String note) {
        SelectStatementProvider selectStatementProvider = select(CourseUserMapper.selectList)
                .from(CourseUserDynamicSqlSupport.courseUser)
                .where(CourseUserDynamicSqlSupport.userId,isEqualTo(userId))
                .and(CourseUserDynamicSqlSupport.courseId,isEqualTo(courseID))
                .build().render(RenderingStrategies.MYBATIS3);
        CourseUser courseUser = courseUserMapper.selectMany(selectStatementProvider).get(0);
        courseUser.setUpdateTime(DateUtils.getNowDate());
        courseUser.setEvaluate(note);
        courseUserMapper.updateByPrimaryKey(courseUser);
    }
}
