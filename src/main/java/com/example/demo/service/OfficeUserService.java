package com.example.demo.service;


import com.example.demo.common.exception.FailedException;
import com.example.demo.common.result.FailedResult;
import com.example.demo.common.util.IdUtils;
import com.example.demo.mapper.OfficeUserDynamicSqlSupport;
import com.example.demo.mapper.OfficeUserMapper;
import com.example.demo.mapper.UserDynamicSqlSupport;
import com.example.demo.mapper.UserMapper;
import com.example.demo.model.OfficeUser;
import com.example.demo.model.User;
import org.mybatis.dynamic.sql.render.RenderingStrategies;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.mybatis.dynamic.sql.SqlBuilder.*;

@Service
public class OfficeUserService {

    private OfficeUserMapper officeUserMapper;

    private UserMapper userMapper;

    @Autowired
    public void setUserMapper(UserMapper userMapper){
        this.userMapper = userMapper;
    }

    @Autowired
    public void setOfficeUserMapper(OfficeUserMapper officeUserMapper){
        this.officeUserMapper = officeUserMapper;
    }

    public int getNumOfOffice(String officeID){
        SelectStatementProvider selectStatementProvider = select(count())
                .from(OfficeUserDynamicSqlSupport.officeUser)
                .where(OfficeUserDynamicSqlSupport.classId,isEqualTo(officeID))
                .build().render(RenderingStrategies.MYBATIS3);
        return (int) officeUserMapper.count(selectStatementProvider);
    }

    public List<String>  getOfficeInfo(String officeID){
        SelectStatementProvider selectStatementProvider = select(UserMapper.selectList)
                .from(UserDynamicSqlSupport.user)
                .leftJoin(OfficeUserDynamicSqlSupport.officeUser)
                .on(UserDynamicSqlSupport.id,equalTo(OfficeUserDynamicSqlSupport.userId))
                .where(OfficeUserDynamicSqlSupport.classId,isEqualTo(officeID))
                .build().render(RenderingStrategies.MYBATIS3);
        List<String> strings = new ArrayList<>();
        if(userMapper.selectMany(selectStatementProvider).size() == 0){
            strings.add("暂无学生加入");
        }else {
            for (User user : userMapper.selectMany(selectStatementProvider)) {
                strings.add(user.getNickName());
            }
        }
        return strings;
    }

    public void joinOffice(String officeID,String userID){
        SelectStatementProvider selectStatementProvider = select(OfficeUserMapper.selectList)
                .from(OfficeUserDynamicSqlSupport.officeUser)
                .where(OfficeUserDynamicSqlSupport.classId,isEqualTo(officeID))
                .and(OfficeUserDynamicSqlSupport.userId,isEqualTo(userID))
                .build().render(RenderingStrategies.MYBATIS3);
        if(officeUserMapper.selectMany(selectStatementProvider).size() != 0){
            throw new FailedException(FailedResult.OFFICE_JOIN_ERROR);
        }
        OfficeUser officeUser = new OfficeUser();
        officeUser.setUserId(userID);
        officeUser.setClassId(officeID);
        officeUser.setId(IdUtils.randomUUID());
        officeUserMapper.insertSelective(officeUser);
    }
}
