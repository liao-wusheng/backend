package com.example.demo.service;


import com.example.demo.common.status.CourseInfoUserStatus;
import com.example.demo.mapper.CourseDynamicSqlSupport;
import com.example.demo.mapper.CourseMapper;
import com.example.demo.mapper.UserDynamicSqlSupport;
import com.example.demo.mapper.UserMapper;
import com.example.demo.model.Course;
import com.example.demo.view.vo.CourseInfoVo;
import org.mybatis.dynamic.sql.render.RenderingStrategies;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.mybatis.dynamic.sql.SqlBuilder.*;

@Service
public class CourseService {

    private CourseMapper courseMapper;

    private UserMapper userMapper;

    @Autowired
    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Autowired
    public void setCourseMapper(CourseMapper courseMapper){
        this.courseMapper = courseMapper;
    }

    public List<Course> getRecentCourse(){
        SelectStatementProvider selectStatementProvider = select(CourseMapper.selectList)
                .from(CourseDynamicSqlSupport.course)
                .orderBy(CourseDynamicSqlSupport.updateTime.descending())
                .limit(4)
                .build().render(RenderingStrategies.MYBATIS3);
        return new ArrayList<>(courseMapper.selectMany(selectStatementProvider));
    }

    public List<CourseInfoVo> getAllCourse(){
        SelectStatementProvider selectStatementProvider = select(CourseMapper.selectList)
                .from(CourseDynamicSqlSupport.course)
                .orderBy(CourseDynamicSqlSupport.updateTime.descending())
                .build().render(RenderingStrategies.MYBATIS3);
        List<CourseInfoVo> courseInfoVos = new ArrayList<>();
        for(Course course : courseMapper.selectMany(selectStatementProvider)){
            CourseInfoVo courseInfoVo = new CourseInfoVo();
            courseInfoVo.setId(course.getId());
            courseInfoVo.setName(course.getName());
            courseInfoVo.setUrl(course.getImageUrl());
            SelectStatementProvider selectStatementProvider1 = select(UserMapper.selectList)
                    .from(UserDynamicSqlSupport.user)
                    .where(UserDynamicSqlSupport.id,isEqualTo(course.getUserId()))
                    .build().render(RenderingStrategies.MYBATIS3);
            courseInfoVo.setTeacher(userMapper.selectMany(selectStatementProvider1).get(0).getNickName());
            courseInfoVo.setHour(course.getClassHour()*10);
            courseInfoVos.add(courseInfoVo);
        }
        return courseInfoVos;
    }

    public Course getCourseInfo(String courseID){
        SelectStatementProvider selectStatementProvider = select(CourseMapper.selectList)
                .from(CourseDynamicSqlSupport.course)
                .where(CourseDynamicSqlSupport.id,isEqualTo(courseID))
                .and(CourseDynamicSqlSupport.state,isEqualTo(CourseInfoUserStatus.OK.getCode()))
                .build().render(RenderingStrategies.MYBATIS3);
        Course course = courseMapper.selectMany(selectStatementProvider).get(0);
        course.setClassHour(course.getClassHour()*10);
        return course;
    }

    public Object getCourseByType(String type) {
        SelectStatementProvider selectStatementProvider = select(CourseMapper.selectList)
                .from(CourseDynamicSqlSupport.course)
                .where(CourseDynamicSqlSupport.type,isEqualTo(type))
                .orderBy(CourseDynamicSqlSupport.updateTime.descending())
                .build().render(RenderingStrategies.MYBATIS3);
        List<CourseInfoVo> courseInfoVos = new ArrayList<>();
        for(Course course : courseMapper.selectMany(selectStatementProvider)){
            CourseInfoVo courseInfoVo = new CourseInfoVo();
            courseInfoVo.setId(course.getId());
            courseInfoVo.setName(course.getName());
            SelectStatementProvider selectStatementProvider1 = select(UserMapper.selectList)
                    .from(UserDynamicSqlSupport.user)
                    .where(UserDynamicSqlSupport.id,isEqualTo(course.getUserId()))
                    .build().render(RenderingStrategies.MYBATIS3);
            courseInfoVo.setTeacher(userMapper.selectMany(selectStatementProvider1).get(0).getNickName());
            courseInfoVo.setHour(course.getClassHour()*10);
            courseInfoVos.add(courseInfoVo);
        }
        return courseInfoVos;
    }

}
