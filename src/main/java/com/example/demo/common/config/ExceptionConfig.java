package com.example.demo.common.config;

import com.example.demo.common.exception.FailedException;
import com.example.demo.common.result.AjaxResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionConfig {

    @ExceptionHandler(FailedException.class)
    public AjaxResult alertExceptionHandler (FailedException failedException){
        return failedException.fail();
    }

}