package com.example.demo.common.constants;

/**
 * 通用常量信息
 * 
 * @author ruoyi
 */
public class Constants
{

    public static final String baseUrl = "..";

    public static final String path = "C:\\mywork\\4207\\fileTest\\";
    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * GBK 字符集
     */
    public static final String GBK = "GBK";

    /**
     * http请求
     */
    public static final String HTTP = "http://";

    /**
     * https请求
     */
    public static final String HTTPS = "https://";

    /**
     * 通用成功标识
     */
    public static final String SUCCESS = "0";

    /**
     * 通用失败标识
     */
    public static final String FAIL = "1";

    /**
     * 登录成功
     */
    public static final String LOGIN_SUCCESS = "Success";

    /**
     * 注销
     */
    public static final String LOGOUT = "Logout";

    /**
     * 注册
     */
    public static final String REGISTER = "Register";

    /**
     * 登录失败
     */
    public static final String LOGIN_FAIL = "Error";

    /**
     * 验证码 redis key
     */
    public static final String CAPTCHA_CODE_KEY = "captcha_codes:";

    /**
     * 登录用户 redis key
     */
    public static final String LOGIN_TOKEN_KEY = "login_tokens:";

    /**
     * 防重提交 redis key
     */
    public static final String REPEAT_SUBMIT_KEY = "repeat_submit:";

    /**
     * 限流 redis key
     */
    public static final String RATE_LIMIT_KEY = "rate_limit:";

    /**
     * 验证码有效期（分钟）
     */
    public static final Integer CAPTCHA_EXPIRATION = 2;

    /**
     * 令牌
     */
    public static final String TOKEN = "token";

    /**
     * 令牌前缀
     */
    public static final String TOKEN_PREFIX = "Bearer ";

    /**
     * 令牌前缀
     */
    public static final String LOGIN_USER_KEY = "login_user_key";

    /**
     * 用户ID
     */
    public static final String JWT_USERID = "userid";

    /**
     * 用户名称
     */
    public static final String JWT_USERNAME = "sub";

    /**
     * 用户头像
     */
    public static final String JWT_AVATAR = "avatar";

    /**
     * 创建时间
     */
    public static final String JWT_CREATED = "created";

    /**
     * 用户权限
     */
    public static final String JWT_AUTHORITIES = "authorities";

    /**
     * 参数管理 cache key
     */
    public static final String SYS_CONFIG_KEY = "sys_config:";

    /**
     * 字典管理 cache key
     */
    public static final String SYS_DICT_KEY = "sys_dict:";

    /**
     * 资源映射路径 前缀
     */
    public static final String RESOURCE_PREFIX = "/profile";

    /**
     * RMI 远程方法调用
     */
    public static final String LOOKUP_RMI = "rmi:";

    /**
     * LDAP 远程方法调用
     */
    public static final String LOOKUP_LDAP = "ldap:";

    /**
     * LEAPS 远程方法调用
     */
    public static final String LOOKUP_LAPS = "laps:";

    /**
     * 定时任务白名单配置（仅允许访问的包名，如其他需要可以自行添加）
     */
    public static final String[] JOB_WHITELIST_STR = { "com.demo" };

    /**
     * 定时任务违规的字符
     */
    public static final String[] JOB_ERROR_STR = { "java.net.URL", "javax.naming.InitialContext", "org.yaml.snakeyaml",
            "org.springframework", "org.apache", "com.demo.common.utils.file" };

    //成功状态码
    public static final int FAILED = 500;//, "未知错误"),

    //参数错误 1001~1999
    public static final int PARAM_IS_INVALID = 1001;//,"参数无效"),
    public static final int PARAM_IS_BLANK = 1002;//,"参数为空"),
    public static final int PARAM_IS_TIME_OUT = 1003;//, "参数失效"),
    public static final int PARAM_TYPE_BIND_ERROR = 1004;//,"参数类型错误"),
    public static final int PARAM_NOT_COMPLETE = 1005;//,"参数缺失"),
    public static final int PARAM_IS_ILLEGAL = 1006;//,"非法参数"),
    public static final int ILLEGAL_OPERATION = 1007;//,"非法操作"),

    //用户错误 2001~2999
    public static final int USER_NOT_LOG_IN = 2001;//,"用户未登录"),
    public static final int USERNAME_OR_PASSWORD_ERROR = 2002;//,"账号密码错误"),
    public static final int USER_ACCOUNT_FORBID = 2003;//,"账户被禁用"),
    public static final int USER_ACCOUNT_EXISTED = 2004;//,"账户已经存在"),
    public static final int USER_NOT_EXIST = 2005;//,"用户不存在"),
    public static final int TOKE_INFO_ERROR = 2006;//,"用户授权信息错误"),
    public static final int PHONE_NUMBER_ALREADY_EXISTS = 2007;//, "手机号已经注册"),
    public static final int ACCOUNT_TO_BE_IMPROVED = 2010;//,"用户信息未完善"),
    public static final int ACCOUNT_TO_LOGOUT = 2011;//,"用户已经注销，无法操作"),
    public static final int TEL_ERROR = 2012;//,"查询不到该手机号"),
    public static final int PASSWORD_ERROR = 2013;//,"旧密码错误"),
    public static final int USERNAME_OR_PASSWORD_NULL = 2014;//用户名密码不能为空
}
