package com.example.demo.common.result;

public enum FailedResult {
    FAILED(500, "未知错误"),

    OFFICE_DO_NOT_EXIST(600,"班级不存在"),
    OFFICE_JOIN_ERROR(601,"重复加入"),

    //参数错误 1001~1999
    PARAM_IS_INVALID(1001,"参数无效"),
    PARAM_IS_EMPTY(1002,"参数为空"),
    PARAM_IS_TIME_OUT(1003, "参数失效"),
    PARAM_TYPE_BIND_ERROR(1004,"参数类型错误"),
    PARAM_NOT_COMPLETE(1005,"参数缺失"),
    PARAM_IS_ILLEGAL(1006,"非法参数"),
    ILLEGAL_OPERATION(1007,"非法操作"),

    //用户错误 2001~2999
    USER_NOT_LOG_IN(2001,"用户未登录"),
    USER_IS_NOT_EXIST(2002,"用户不存在"),
    USER_ACCOUNT_BAN(2003,"账户被禁用"),
    USER_ACCOUNT_EXISTED(2004,"账户已经存在"),
    TOKE_INFO_ERROR(2006,"用户授权信息错误"),
    PHONE_NUMBER_ALREADY_EXISTS(2007, "手机号已经注册"),
    USER_NICKNAME_ALREADY_EXISTS(2008, "操作昵称已经存在"),
    STU_NUM_EXISTS(2009,"学号已经存在"),
    ACCOUNT_TO_BE_IMPROVED(2010,"用户信息未完善"),
    ACCOUNT_TO_LOGOUT(2011,"用户已经注销，无法操作"),
    TEL_ERROR(2012,"查询不到该手机号"),
    SELECT_USER_INFO_FAILED(2013,"查询用户信息异常"),
    UPDATE_USERINFO_FAIL(2014,"修改个人信息失败"),
    PASSWORD_ERROR(2015,"旧密码错误"),
    USER_IS_DELETE(2016,"账户被删除"),
    USERNAME_PASSWORD_ERROR(2017,"账号密码错误"),
    REGISTER_FAILED(2018,"注册失败"),

    SELECT_DEPARTMENT_INFO_FAILED(6001,"获取部门信息异常"),

    FILE_UPLOAD_FAILED(3001, "文件上传失败"),
    FILE_STORAGE_FILED(3002, "文件存储失败"),
    FILE_DOWNLOAD_FAILED(3003, "文件下载失败"),
    FILE_DELETE_FAILED(3004, "文件删除失败"),
    FILE_NUM_UPPER_LIMIT(3005, "上传文件数已达上限"),
    FILE_PATH_ERROR(3006,"文件存储路径错误"),
    FILE_SELECT_ERROR(3007,"无法查找到文件"),

    PERMISSION_REJECTED(4001, "无权访问"),
    REPEAT_OPERATION(4003,"重复操作"),
    UPDATE_USER_ROLE_IS_NOT_PERMITTED(4004,"不允许删除普通用户权限"),

    DEPARTMENT_FIND_FAILED(5001,"该学院专业不存在"),
    DEPARTMENT_IS_EXIST(5002,"该专业名已经存在，请修改名称"),

    SYS_CONFIG_KEY_EXISTS(7001, "配置已经存在，请执行更新操作"),
    SYS_CONFIG_NOT_EXISTS(7002, "更新失败，未添加系统配置"),
    SYS_CONFIG_KEY_NOT_EXISTS(7003, "更新失败，不存在该配置，考虑添加后更新"),

    TEACH_OFFICE_SET_UP_FAILED(9001,"创建教研室失败"),

    SYSTEM_ERROR(500,"服务器异常"),
    UPDATE_ERROR(500,"更新数据失败"),
    INSERT_ERROR(500,"添加数据失败"),
    DELETE_ERROR(500,"删除数据失败"),
    ;


    private final Integer code;
    private final String message;

    FailedResult(Integer code, String message) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public Integer getCode() {
        return code;
    }

    public Integer code(){
        return this.code;
    }

    public String message(){
        return this.message;
    }
}