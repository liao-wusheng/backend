package com.example.demo.common.result;

public class ResultString {
    //成功状态码
    public static String SUCCESS = "操作成功";//
    public static String FAILED = "未知错误";

    //参数错误 1001~1999
    public static String PARAM_IS_INVALID = "参数无效";
    public static String PARAM_IS_BLANK = "参数为空";
    public static String PARAM_IS_TIME_OUT =  "参数失效";
    public static String PARAM_TYPE_BIND_ERROR = "参数类型错误";
    public static String PARAM_NOT_COMPLETE = "参数缺失";
    public static String PARAM_IS_ILLEGAL = "非法参数";
    public static String ILLEGAL_OPERATION = "非法操作";

    //用户错误 2001~2999
    public static String USER_NOT_LOG_IN = "用户未登录";
    public static String PASSWORD_ERROR = "旧密码错误";
    public static String USERNAME_OR_PASSWORD_ERROR = "账号密码错误，或账号被禁用";
    public static String USER_ACCOUNT_FORBID = "账户被禁用";
    public static String USER_ACCOUNT_EXISTED = "账户已经存在";
    public static String USER_NOT_EXIST = "用户不存在";
    public static String TOKE_INFO_ERROR = "用户授权信息错误";
    public static String PHONE_NUMBER_ALREADY_EXISTS = "手机号已经注册";
    public static String ACCOUNT_TO_BE_IMPROVED = "用户信息未完善";
    public static String ACCOUNT_TO_LOGOUT = "用户已经注销，无法操作";
    public static String TEL_ERROR = "查询不到该手机号";
    public static String USERNAME_OR_PASSWORD_NULL = "用户名密码不能为空";
}