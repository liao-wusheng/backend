package com.example.demo.common.exception;

import com.example.demo.common.result.AjaxResult;
import com.example.demo.common.result.FailedResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 业务异常
 * 
 * @author ruoyi
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public final class FailedException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    private FailedResult failedResult;


    public AjaxResult fail() {
        return AjaxResult.failure(this.failedResult);
    }
}