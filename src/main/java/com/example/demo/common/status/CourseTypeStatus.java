package com.example.demo.common.status;

public enum CourseTypeStatus {
    DONE("1","已完成"),OK("2","学习中"),DISABLE("3","未开始");
    private  final String code;
    private  final String message;

    public  String getCode() {
        return code;
    }

    public  String getMessage() {
        return message;
    }
    CourseTypeStatus(String code, String message){
        this.code = code;
        this.message = message;
    }
}
