package com.example.demo.common.status;

public enum UserStatus
{
    DELETE("0","删除"),OK("1","正常"),DISABLE("2","停用");
    private  final String code;
    private  final String message;

    public  String getCode() {
        return code;
    }

    public  String getMessage() {
        return message;
    }
    UserStatus(String code,String message){
        this.code = code;
        this.message = message;
    }
}
