package com.example.demo.common.status;

public enum DepartmentStatus {
    OK("1"),DISABLE("2");
    private  String code;

    public String getCode() {
        return code;
    }
    DepartmentStatus(String code){
        this.code = code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
