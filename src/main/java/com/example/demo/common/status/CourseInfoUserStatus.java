package com.example.demo.common.status;

public enum CourseInfoUserStatus {
    DONE(1,"已完成"),OK(1,"学习中"),DISABLE(2,"未开始");
    private  final int code;
    private  final String message;

    public  int getCode() {
        return code;
    }

    public  String getMessage() {
        return message;
    }
    CourseInfoUserStatus(int code, String message){
        this.code = code;
        this.message = message;
    }
}
