package com.example.demo.common.status;

import java.util.Arrays;
import java.util.List;

public enum FileTypeStatus {


    FOLDER("1"), OFFICE("2"), AUDIO("3"), VIDEO("4"), PICTURE("5"), ANOTHER("6");

    private static final String[] OFFICE_TYPE = {"doc", "docx", "txt", "xls", "xlsx", "ppt", "pptx", "wps", "pdf"};

    private static final String[] AUDIO_TYPE = {"wav", "aif", "au", "mp3"};

    private static final String[] VIDEO_TYPE = {"avi", "mpg", "mp4"};

    private static final String[] PICTURE_TYPE = {"bmp", "gif", "jpg", "pic", "png", "tif"};

    private String type;

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    FileTypeStatus(String type) {
        this.type = type;
    }

    public static String selectType(String suffix) {
        if (suffix.equals(FOLDER.getType())) {
            return FOLDER.type;
        } else if (Arrays.asList(OFFICE_TYPE).contains(suffix)) {
            return OFFICE.getType();
        } else if (Arrays.asList(AUDIO_TYPE).contains(suffix)) {
            return AUDIO.getType();
        } else if (Arrays.asList(VIDEO_TYPE).contains(suffix)) {
            return VIDEO.getType();
        } else if (Arrays.asList(PICTURE_TYPE).contains(suffix)) {
            return PICTURE.getType();
        } else {
            return ANOTHER.getType();
        }
    }
}
