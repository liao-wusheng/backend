package com.example.demo.common.status;

public enum FileStateStatus {
    DELETE("0"),OK("1"),DISABLE("2"),
    ;

    private String state;

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    FileStateStatus(String state){
        this.state = state;
    }
}
