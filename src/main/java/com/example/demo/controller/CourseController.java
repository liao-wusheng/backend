package com.example.demo.controller;


import com.example.demo.common.result.AjaxResult;
import com.example.demo.common.util.SecurityUtils;
import com.example.demo.service.CourseInfoService;
import com.example.demo.service.CourseInfoUserService;
import com.example.demo.service.CourseService;
import com.example.demo.service.CourseUserService;
import com.example.demo.view.vo.EvaluateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Validated
@Api(tags = "课程页面")
@RequestMapping("/course")
public class CourseController {

    private CourseService courseService;

    private CourseUserService courseUserService;

    private CourseInfoService courseInfoService;

    private CourseInfoUserService courseInfoUserService;

    @Autowired
    public void setCourseInfoService(CourseInfoService courseInfoService) {
        this.courseInfoService = courseInfoService;
    }

    @Autowired
    public void setCourseInfoUserService(CourseInfoUserService courseInfoUserService) {
        this.courseInfoUserService = courseInfoUserService;
    }

    @Autowired
    public void setCourseService(CourseService courseService) {
        this.courseService = courseService;
    }

    @Autowired
    public void setCourseUserService(CourseUserService courseUserService) {
        this.courseUserService = courseUserService;
    }

    @ApiOperation("获取所有课程")
    @GetMapping("getAllCourse")
    public AjaxResult getAllCourse() {
        AjaxResult ajaxResult = AjaxResult.success();
        ajaxResult.put("list", courseService.getAllCourse());
        return ajaxResult;
    }

    @ApiOperation("根据课程id获取课程详细信息以及课时目录")
    @PostMapping("getCourseInfo")
    public AjaxResult getCourseInfo(@RequestParam("courseID") String courseID) {
        if (!courseUserService.IsJoin(courseID, SecurityUtils.getUserId())) {
            courseUserService.Join(courseID, SecurityUtils.getUserId());
            courseInfoUserService.Join(courseID, SecurityUtils.getUserId());
        }
        AjaxResult ajaxResult = AjaxResult.success();
        ajaxResult.put("rate", courseInfoUserService.getRateOfCourse(SecurityUtils.getUserId(), courseID));
        ajaxResult.put("courseDetails", courseInfoUserService.getCourseDetails(SecurityUtils.getUserId(), courseID));
        ajaxResult.put("courseInfo", courseService.getCourseInfo(courseID));
        return ajaxResult;
    }


    @ApiOperation("根据课程id获取课程评价")
    @PostMapping("getCourseEvaluate")
    public AjaxResult getCourseEvaluate(@RequestParam("courseID") String courseID) {
        AjaxResult ajaxResult = AjaxResult.success();
        ajaxResult.put("list", courseUserService.getCourseEvaluate(courseID));
        return ajaxResult;
    }

    @ApiOperation("根据id获取课程笔记")
    @PostMapping("getCourseNote")
    public AjaxResult getCourseNote(@RequestParam("courseID") String courseID) {
        AjaxResult ajaxResult = AjaxResult.success();
        ajaxResult.put("list", courseUserService.getCourseNote(courseID));
        return ajaxResult;
    }

    @ApiOperation("根据id获取具体课时链接和我的笔记以及他人笔记")
    @PostMapping("/getUrlNote")
    public AjaxResult getUrlNote(@RequestParam("courseInfoID") String courseInfoID) {
        AjaxResult ajaxResult = AjaxResult.success();
        ajaxResult.put("myNote", courseInfoUserService.getNote(courseInfoID, SecurityUtils.getUserId()));
        ajaxResult.put("Info", courseInfoService.getInfo(courseInfoID));
        ajaxResult.put("anotherNote", courseInfoUserService.getNote(courseInfoID, "jjjjjjj"));
        return ajaxResult;
    }

    @ApiOperation("根据课程类别获取课程列表")
    @PostMapping("getCourseByType")
    private AjaxResult getCourseByType(@RequestParam("type") String type) {
        AjaxResult ajaxResult = AjaxResult.success();
        ajaxResult.put("list", courseService.getCourseByType(type));
        return ajaxResult;
    }

    @ApiOperation("对课程进行评价")
    @PostMapping("evaluateCourse")
    private AjaxResult evaluateCourse(@RequestBody EvaluateVo evaluateVo) {
        if (!courseUserService.IsJoin(evaluateVo.getCourseID(), SecurityUtils.getUserId())) {
            courseUserService.Join(evaluateVo.getCourseID(), SecurityUtils.getUserId());
            courseInfoUserService.Join(evaluateVo.getCourseID(), SecurityUtils.getUserId());
        }
        courseUserService.evaluateCourse(SecurityUtils.getUserId(), evaluateVo.getCourseID(), evaluateVo.getString());
        return AjaxResult.success();
    }

    @ApiOperation("写课程学习笔记")
    @PostMapping("noteCourse")
    private AjaxResult noteCourse(@RequestBody EvaluateVo evaluateVo) {
        if (!courseUserService.IsJoin(evaluateVo.getCourseID(), SecurityUtils.getUserId())) {
            courseUserService.Join(evaluateVo.getCourseID(), SecurityUtils.getUserId());
            courseInfoUserService.Join(evaluateVo.getCourseID(), SecurityUtils.getUserId());
        }
        courseUserService.noteCourse(SecurityUtils.getUserId(), evaluateVo.getCourseID(), evaluateVo.getString());
        return AjaxResult.success();
    }

    @ApiOperation("写课时学习笔记")
    @PostMapping("noteCourseInfo")
    private AjaxResult noteCourseInfo(@RequestBody EvaluateVo evaluateVo) {
        courseInfoUserService.noteCourseInfo(SecurityUtils.getUserId(), evaluateVo.getCourseID(), evaluateVo.getString());
        return AjaxResult.success();
    }
}
