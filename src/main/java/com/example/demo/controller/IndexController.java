package com.example.demo.controller;


import com.example.demo.common.result.AjaxResult;
import com.example.demo.service.CourseService;
import com.example.demo.service.OfficeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/index")
@Validated
@Api(tags = "小程序主页界面")
public class IndexController {

    private OfficeService officeService;

    private CourseService courseService;

    @Autowired
    public void setCourseService(CourseService courseService){
        this.courseService = courseService;
    }

    @Autowired
    public void setOfficeService(OfficeService officeService){
        this.officeService = officeService;
    }

    @ApiOperation("获取首页四个班级")
    @GetMapping("getRecentOffice")
    public AjaxResult getRecentOffice(){
        AjaxResult ajaxResult = AjaxResult.success();
        ajaxResult.put("officeInfo",officeService.getRecentOffice());
        return ajaxResult;
    }

    @ApiOperation("获取首页四个课程")
    @GetMapping("getRecentCourse")
    public AjaxResult getRecentCourse(){
        AjaxResult ajaxResult = AjaxResult.success();
        ajaxResult.put("courseInfo",courseService.getRecentCourse());
        return ajaxResult;
    }
}
