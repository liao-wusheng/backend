package com.example.demo.controller;


import com.example.demo.common.result.AjaxResult;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "用户模块")
@Validated
@RequestMapping("/user")
public class UserController {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @ApiOperation("修改用户个人信息")
    @PostMapping("/updateUserInfo")
    public AjaxResult updateUserInfo(@RequestBody User user){
        userService.updateUserInfo(user);
        return AjaxResult.success();
    }
}
