package com.example.demo.controller;


import com.example.demo.common.result.AjaxResult;
import com.example.demo.common.result.FailedResult;
import com.example.demo.common.util.SecurityUtils;
import com.example.demo.service.OfficeService;
import com.example.demo.service.OfficeUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Validated
@RequestMapping("/office")
@Api(tags = "班级页面")
public class OfficeController {

    private OfficeService officeService;

    private OfficeUserService officeUserService;

    @Autowired
    public void setOfficeUserService(OfficeUserService officeUserService){
        this.officeUserService = officeUserService;
    }

    @Autowired
    public void setOfficeService(OfficeService officeService) {
        this.officeService = officeService;
    }

    @ApiOperation("获取所有班级")
    @GetMapping("/getAllOffice")
    public AjaxResult getAllOffice(){
        AjaxResult ajaxResult = AjaxResult.success();
        ajaxResult.put("list",officeService.getAllOffice());
        return ajaxResult;
    }

    @ApiOperation("获取班级成员信息")
    @PostMapping("/getOfficeInfo")
    public AjaxResult getOfficeInfo(@RequestParam("officeID") String officeID){
        AjaxResult ajaxResult = AjaxResult.success();
        if(officeService.officeExist(officeID)) {
            ajaxResult.put("officeInfo", officeUserService.getOfficeInfo(officeID));
        }else {
            return AjaxResult.failure(FailedResult.OFFICE_DO_NOT_EXIST);
        }
        return ajaxResult;
    }

    @ApiOperation("学生加入班级")
    @PostMapping("/joinOffice")
    public AjaxResult joinOffice(@RequestParam("officeID") String officeID){
        if(officeService.officeExist(officeID)) {
            officeUserService.joinOffice(officeID, SecurityUtils.getLoginUser().getUser().getId());
            return AjaxResult.success();
        }else {
            return AjaxResult.failure(FailedResult.OFFICE_DO_NOT_EXIST);
        }
    }
}
