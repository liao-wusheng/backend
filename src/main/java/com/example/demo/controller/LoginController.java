package com.example.demo.controller;


import com.example.demo.common.result.AjaxResult;
import com.example.demo.common.result.FailedResult;
import com.example.demo.common.util.SecurityUtils;
import com.example.demo.common.util.StringUtils;
import com.example.demo.model.User;
import com.example.demo.service.LoginService;
import com.example.demo.service.UserRoleService;
import com.example.demo.service.UserService;
import com.example.demo.view.dto.LoginDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Validated
@Api(tags = "登录模块")
public class LoginController {

    private LoginService loginService;

    private UserRoleService userRoleService;

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService){
        this.userService = userService;
    }

    @Autowired
    public void setUserRoleService(UserRoleService userRoleService){
        this.userRoleService = userRoleService;
    }

    @Autowired
    public void setLoginService(LoginService loginService){
        this.loginService = loginService;
    }

    @ApiOperation("获取登录token")
    @PostMapping("/getToken")
    public AjaxResult getToken(@RequestBody LoginDto loginDto){
        if(StringUtils.isEmpty(loginDto.getPhone()) || StringUtils.isEmpty(loginDto.getPassword())){
            return AjaxResult.failure(FailedResult.PARAM_IS_EMPTY);
        }else {
            AjaxResult ajaxResult = AjaxResult.success();
            ajaxResult.put("token" , loginService.login(loginDto.getPhone(),loginDto.getPassword()));
            return ajaxResult;
        }
    }

    @ApiOperation("获取用户信息")
    @GetMapping("/getUserInfo")
    public AjaxResult getUser(){
        AjaxResult ajaxResult = AjaxResult.success();
        User user = SecurityUtils.getLoginUser().getUser();
        ajaxResult.put("userInfo" , user);
        ajaxResult.put("role" , userRoleService.selectRoleByUserID(user.getId()));
        return ajaxResult;
    }

    @ApiOperation("注册")
    @PostMapping("/register")
    public AjaxResult register(@RequestBody LoginDto loginDto){
        if(userService.selectUserByUserName(loginDto.getPhone()) != null){
            return AjaxResult.failure(FailedResult.REGISTER_FAILED);
        }else {
            User user = userService.register(loginDto.getPhone(), loginDto.getPassword());
            user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
            AjaxResult ajaxResult = AjaxResult.success();
            ajaxResult.put("userInfo" , user);
            return ajaxResult;
        }
    }

    @PostMapping("/updatePwd")
    @ApiOperation("修改密码")
    public AjaxResult updatePwd(@RequestBody LoginDto loginDto){
        if(userService.selectUserByUserName(loginDto.getPhone()) != null){
            return AjaxResult.failure(FailedResult.REGISTER_FAILED);
        }else {
            User user = userService.selectUserByUserName(loginDto.getPhone());
            user.setPassword(loginDto.getPassword());
            userService.updatePwd(user);
            return AjaxResult.success();
        }
    }
}
