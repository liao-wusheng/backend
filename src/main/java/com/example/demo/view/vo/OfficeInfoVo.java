package com.example.demo.view.vo;


import com.example.demo.model.Office;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OfficeInfoVo {
    private Office office;
    private int Num_Student;
}
