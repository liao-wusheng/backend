package com.example.demo.view.vo;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EvaluateVo {

    private String courseID;
    private String string;

}
