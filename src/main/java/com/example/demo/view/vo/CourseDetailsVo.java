package com.example.demo.view.vo;


import com.example.demo.model.CourseInfo;
import com.example.demo.model.CourseInfoUser;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor

public class CourseDetailsVo {
    private String id;
    private String url;
    private String name;
    private String state;
}
