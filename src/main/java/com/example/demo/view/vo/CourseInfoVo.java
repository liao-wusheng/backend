package com.example.demo.view.vo;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.naming.Name;

@Data
@NoArgsConstructor
public class CourseInfoVo {

    private String id;
    private String name;
    private String teacher;
    private int hour;
    private String url;

}
