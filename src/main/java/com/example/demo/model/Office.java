package com.example.demo.model;

import java.util.Date;
import javax.annotation.Generated;

public class Office {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.272+08:00", comments="Source field: office.id")
    private String id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.275+08:00", comments="Source field: office.name")
    private String name;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.275+08:00", comments="Source field: office.teacher")
    private String teacher;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.275+08:00", comments="Source field: office.create_time")
    private Date createTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.275+08:00", comments="Source field: office.update_time")
    private Date updateTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.275+08:00", comments="Source field: office.img_url")
    private String imgUrl;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.276+08:00", comments="Source field: office.description")
    private String description;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.275+08:00", comments="Source field: office.id")
    public String getId() {
        return id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.275+08:00", comments="Source field: office.id")
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.275+08:00", comments="Source field: office.name")
    public String getName() {
        return name;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.275+08:00", comments="Source field: office.name")
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.275+08:00", comments="Source field: office.teacher")
    public String getTeacher() {
        return teacher;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.275+08:00", comments="Source field: office.teacher")
    public void setTeacher(String teacher) {
        this.teacher = teacher == null ? null : teacher.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.275+08:00", comments="Source field: office.create_time")
    public Date getCreateTime() {
        return createTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.275+08:00", comments="Source field: office.create_time")
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.275+08:00", comments="Source field: office.update_time")
    public Date getUpdateTime() {
        return updateTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.275+08:00", comments="Source field: office.update_time")
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.275+08:00", comments="Source field: office.img_url")
    public String getImgUrl() {
        return imgUrl;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.276+08:00", comments="Source field: office.img_url")
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl == null ? null : imgUrl.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.276+08:00", comments="Source field: office.description")
    public String getDescription() {
        return description;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.276+08:00", comments="Source field: office.description")
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }
}