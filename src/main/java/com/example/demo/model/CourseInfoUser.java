package com.example.demo.model;

import java.util.Date;
import javax.annotation.Generated;

public class CourseInfoUser {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.id")
    private String id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.course_info_id")
    private String courseInfoId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.state")
    private Integer state;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.create_time")
    private Date createTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.update_time")
    private Date updateTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.note")
    private String note;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.user_id")
    private String userId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.id")
    public String getId() {
        return id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.id")
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.course_info_id")
    public String getCourseInfoId() {
        return courseInfoId;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.course_info_id")
    public void setCourseInfoId(String courseInfoId) {
        this.courseInfoId = courseInfoId == null ? null : courseInfoId.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.state")
    public Integer getState() {
        return state;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.state")
    public void setState(Integer state) {
        this.state = state;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.create_time")
    public Date getCreateTime() {
        return createTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.create_time")
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.update_time")
    public Date getUpdateTime() {
        return updateTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.update_time")
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.note")
    public String getNote() {
        return note;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.note")
    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.user_id")
    public String getUserId() {
        return userId;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.user_id")
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }
}