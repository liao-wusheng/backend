package com.example.demo.model;

import java.util.Date;
import javax.annotation.Generated;

public class CourseInfo {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.id")
    private String id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.course_id")
    private String courseId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.course_hour")
    private Integer courseHour;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.course_url")
    private String courseUrl;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.course_name")
    private String courseName;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.create_time")
    private Date createTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.update_time")
    private Date updateTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.id")
    public String getId() {
        return id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.id")
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.course_id")
    public String getCourseId() {
        return courseId;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.course_id")
    public void setCourseId(String courseId) {
        this.courseId = courseId == null ? null : courseId.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.course_hour")
    public Integer getCourseHour() {
        return courseHour;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.course_hour")
    public void setCourseHour(Integer courseHour) {
        this.courseHour = courseHour;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.course_url")
    public String getCourseUrl() {
        return courseUrl;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.course_url")
    public void setCourseUrl(String courseUrl) {
        this.courseUrl = courseUrl == null ? null : courseUrl.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.course_name")
    public String getCourseName() {
        return courseName;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.course_name")
    public void setCourseName(String courseName) {
        this.courseName = courseName == null ? null : courseName.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.create_time")
    public Date getCreateTime() {
        return createTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.create_time")
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.update_time")
    public Date getUpdateTime() {
        return updateTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.94+08:00", comments="Source field: course_info.update_time")
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}