package com.example.demo.model;

import java.util.Date;
import javax.annotation.Generated;

public class Course {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.324+08:00", comments="Source field: course.id")
    private String id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.image_url")
    private String imageUrl;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.name")
    private String name;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.user_id")
    private String userId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.class_hour")
    private Integer classHour;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.create_time")
    private Date createTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.update_time")
    private Date updateTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.type")
    private String type;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.state")
    private Integer state;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.326+08:00", comments="Source field: course.description")
    private String description;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.id")
    public String getId() {
        return id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.id")
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.image_url")
    public String getImageUrl() {
        return imageUrl;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.image_url")
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl == null ? null : imageUrl.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.name")
    public String getName() {
        return name;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.name")
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.user_id")
    public String getUserId() {
        return userId;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.user_id")
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.class_hour")
    public Integer getClassHour() {
        return classHour;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.class_hour")
    public void setClassHour(Integer classHour) {
        this.classHour = classHour;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.create_time")
    public Date getCreateTime() {
        return createTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.create_time")
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.update_time")
    public Date getUpdateTime() {
        return updateTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.update_time")
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.type")
    public String getType() {
        return type;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.type")
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.325+08:00", comments="Source field: course.state")
    public Integer getState() {
        return state;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.326+08:00", comments="Source field: course.state")
    public void setState(Integer state) {
        this.state = state;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.326+08:00", comments="Source field: course.description")
    public String getDescription() {
        return description;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.326+08:00", comments="Source field: course.description")
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }
}