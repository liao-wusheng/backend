package com.example.demo.model;

import java.util.Date;
import javax.annotation.Generated;

public class UserRole {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.userID")
    private String userid;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.roleID")
    private Long roleid;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.state")
    private String state;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.create_time")
    private Date createTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.update_time")
    private Date updateTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.userID")
    public String getUserid() {
        return userid;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.userID")
    public void setUserid(String userid) {
        this.userid = userid == null ? null : userid.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.roleID")
    public Long getRoleid() {
        return roleid;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.roleID")
    public void setRoleid(Long roleid) {
        this.roleid = roleid;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.state")
    public String getState() {
        return state;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.state")
    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.create_time")
    public Date getCreateTime() {
        return createTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.create_time")
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.update_time")
    public Date getUpdateTime() {
        return updateTime;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.update_time")
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}