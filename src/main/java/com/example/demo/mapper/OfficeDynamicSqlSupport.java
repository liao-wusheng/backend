package com.example.demo.mapper;

import java.sql.JDBCType;
import java.util.Date;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class OfficeDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.278+08:00", comments="Source Table: office")
    public static final Office office = new Office();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.278+08:00", comments="Source field: office.id")
    public static final SqlColumn<String> id = office.id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.279+08:00", comments="Source field: office.name")
    public static final SqlColumn<String> name = office.name;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.279+08:00", comments="Source field: office.teacher")
    public static final SqlColumn<String> teacher = office.teacher;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.279+08:00", comments="Source field: office.create_time")
    public static final SqlColumn<Date> createTime = office.createTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.279+08:00", comments="Source field: office.update_time")
    public static final SqlColumn<Date> updateTime = office.updateTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.279+08:00", comments="Source field: office.img_url")
    public static final SqlColumn<String> imgUrl = office.imgUrl;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.279+08:00", comments="Source field: office.description")
    public static final SqlColumn<String> description = office.description;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.278+08:00", comments="Source Table: office")
    public static final class Office extends SqlTable {
        public final SqlColumn<String> id = column("id", JDBCType.VARCHAR);

        public final SqlColumn<String> name = column("name", JDBCType.VARCHAR);

        public final SqlColumn<String> teacher = column("teacher", JDBCType.VARCHAR);

        public final SqlColumn<Date> createTime = column("create_time", JDBCType.TIMESTAMP);

        public final SqlColumn<Date> updateTime = column("update_time", JDBCType.TIMESTAMP);

        public final SqlColumn<String> imgUrl = column("img_url", JDBCType.VARCHAR);

        public final SqlColumn<String> description = column("description", JDBCType.LONGVARCHAR);

        public Office() {
            super("office");
        }
    }
}