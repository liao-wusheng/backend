package com.example.demo.mapper;

import java.sql.JDBCType;
import java.util.Date;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class CourseInfoUserDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source Table: course_info_user")
    public static final CourseInfoUser courseInfoUser = new CourseInfoUser();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.id")
    public static final SqlColumn<String> id = courseInfoUser.id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.course_info_id")
    public static final SqlColumn<String> courseInfoId = courseInfoUser.courseInfoId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.state")
    public static final SqlColumn<Integer> state = courseInfoUser.state;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source field: course_info_user.create_time")
    public static final SqlColumn<Date> createTime = courseInfoUser.createTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source field: course_info_user.update_time")
    public static final SqlColumn<Date> updateTime = courseInfoUser.updateTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source field: course_info_user.note")
    public static final SqlColumn<String> note = courseInfoUser.note;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source field: course_info_user.user_id")
    public static final SqlColumn<String> userId = courseInfoUser.userId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.484+08:00", comments="Source Table: course_info_user")
    public static final class CourseInfoUser extends SqlTable {
        public final SqlColumn<String> id = column("id", JDBCType.VARCHAR);

        public final SqlColumn<String> courseInfoId = column("course_info_id", JDBCType.VARCHAR);

        public final SqlColumn<Integer> state = column("state", JDBCType.INTEGER);

        public final SqlColumn<Date> createTime = column("create_time", JDBCType.TIMESTAMP);

        public final SqlColumn<Date> updateTime = column("update_time", JDBCType.TIMESTAMP);

        public final SqlColumn<String> note = column("note", JDBCType.VARCHAR);

        public final SqlColumn<String> userId = column("user_id", JDBCType.VARCHAR);

        public CourseInfoUser() {
            super("course_info_user");
        }
    }
}