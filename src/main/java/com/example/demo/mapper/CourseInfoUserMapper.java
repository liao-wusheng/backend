package com.example.demo.mapper;

import static com.example.demo.mapper.CourseInfoUserDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.*;

import com.example.demo.model.CourseInfoUser;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.annotation.Generated;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
import org.mybatis.dynamic.sql.insert.render.MultiRowInsertStatementProvider;
import org.mybatis.dynamic.sql.select.CountDSLCompleter;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.UpdateDSL;
import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
import org.mybatis.dynamic.sql.update.UpdateModel;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;

@Mapper
public interface CourseInfoUserMapper {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    BasicColumn[] selectList = BasicColumn.columnList(id, courseInfoId, state, createTime, updateTime, note, userId);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    long count(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    @DeleteProvider(type=SqlProviderAdapter.class, method="delete")
    int delete(DeleteStatementProvider deleteStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    @InsertProvider(type=SqlProviderAdapter.class, method="insert")
    int insert(InsertStatementProvider<CourseInfoUser> insertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    @InsertProvider(type=SqlProviderAdapter.class, method="insertMultiple")
    int insertMultiple(MultiRowInsertStatementProvider<CourseInfoUser> multipleInsertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @ResultMap("CourseInfoUserResult")
    Optional<CourseInfoUser> selectOne(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="CourseInfoUserResult", value = {
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="course_info_id", property="courseInfoId", jdbcType=JdbcType.VARCHAR),
        @Result(column="state", property="state", jdbcType=JdbcType.INTEGER),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="note", property="note", jdbcType=JdbcType.VARCHAR),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.VARCHAR)
    })
    List<CourseInfoUser> selectMany(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    @UpdateProvider(type=SqlProviderAdapter.class, method="update")
    int update(UpdateStatementProvider updateStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    default long count(CountDSLCompleter completer) {
        return MyBatis3Utils.countFrom(this::count, courseInfoUser, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    default int delete(DeleteDSLCompleter completer) {
        return MyBatis3Utils.deleteFrom(this::delete, courseInfoUser, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    default int deleteByPrimaryKey(String id_) {
        return delete(c -> 
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    default int insert(CourseInfoUser record) {
        return MyBatis3Utils.insert(this::insert, record, courseInfoUser, c ->
            c.map(id).toProperty("id")
            .map(courseInfoId).toProperty("courseInfoId")
            .map(state).toProperty("state")
            .map(createTime).toProperty("createTime")
            .map(updateTime).toProperty("updateTime")
            .map(note).toProperty("note")
            .map(userId).toProperty("userId")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    default int insertMultiple(Collection<CourseInfoUser> records) {
        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, courseInfoUser, c ->
            c.map(id).toProperty("id")
            .map(courseInfoId).toProperty("courseInfoId")
            .map(state).toProperty("state")
            .map(createTime).toProperty("createTime")
            .map(updateTime).toProperty("updateTime")
            .map(note).toProperty("note")
            .map(userId).toProperty("userId")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    default int insertSelective(CourseInfoUser record) {
        return MyBatis3Utils.insert(this::insert, record, courseInfoUser, c ->
            c.map(id).toPropertyWhenPresent("id", record::getId)
            .map(courseInfoId).toPropertyWhenPresent("courseInfoId", record::getCourseInfoId)
            .map(state).toPropertyWhenPresent("state", record::getState)
            .map(createTime).toPropertyWhenPresent("createTime", record::getCreateTime)
            .map(updateTime).toPropertyWhenPresent("updateTime", record::getUpdateTime)
            .map(note).toPropertyWhenPresent("note", record::getNote)
            .map(userId).toPropertyWhenPresent("userId", record::getUserId)
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    default Optional<CourseInfoUser> selectOne(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectOne(this::selectOne, selectList, courseInfoUser, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    default List<CourseInfoUser> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, courseInfoUser, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    default List<CourseInfoUser> selectDistinct(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, courseInfoUser, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    default Optional<CourseInfoUser> selectByPrimaryKey(String id_) {
        return selectOne(c ->
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    default int update(UpdateDSLCompleter completer) {
        return MyBatis3Utils.update(this::update, courseInfoUser, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.485+08:00", comments="Source Table: course_info_user")
    static UpdateDSL<UpdateModel> updateAllColumns(CourseInfoUser record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalTo(record::getId)
                .set(courseInfoId).equalTo(record::getCourseInfoId)
                .set(state).equalTo(record::getState)
                .set(createTime).equalTo(record::getCreateTime)
                .set(updateTime).equalTo(record::getUpdateTime)
                .set(note).equalTo(record::getNote)
                .set(userId).equalTo(record::getUserId);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.486+08:00", comments="Source Table: course_info_user")
    static UpdateDSL<UpdateModel> updateSelectiveColumns(CourseInfoUser record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalToWhenPresent(record::getId)
                .set(courseInfoId).equalToWhenPresent(record::getCourseInfoId)
                .set(state).equalToWhenPresent(record::getState)
                .set(createTime).equalToWhenPresent(record::getCreateTime)
                .set(updateTime).equalToWhenPresent(record::getUpdateTime)
                .set(note).equalToWhenPresent(record::getNote)
                .set(userId).equalToWhenPresent(record::getUserId);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.486+08:00", comments="Source Table: course_info_user")
    default int updateByPrimaryKey(CourseInfoUser record) {
        return update(c ->
            c.set(courseInfoId).equalTo(record::getCourseInfoId)
            .set(state).equalTo(record::getState)
            .set(createTime).equalTo(record::getCreateTime)
            .set(updateTime).equalTo(record::getUpdateTime)
            .set(note).equalTo(record::getNote)
            .set(userId).equalTo(record::getUserId)
            .where(id, isEqualTo(record::getId))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T01:04:54.486+08:00", comments="Source Table: course_info_user")
    default int updateByPrimaryKeySelective(CourseInfoUser record) {
        return update(c ->
            c.set(courseInfoId).equalToWhenPresent(record::getCourseInfoId)
            .set(state).equalToWhenPresent(record::getState)
            .set(createTime).equalToWhenPresent(record::getCreateTime)
            .set(updateTime).equalToWhenPresent(record::getUpdateTime)
            .set(note).equalToWhenPresent(record::getNote)
            .set(userId).equalToWhenPresent(record::getUserId)
            .where(id, isEqualTo(record::getId))
        );
    }
}