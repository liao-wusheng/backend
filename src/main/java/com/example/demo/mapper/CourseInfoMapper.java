package com.example.demo.mapper;

import static com.example.demo.mapper.CourseInfoDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.*;

import com.example.demo.model.CourseInfo;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.annotation.Generated;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
import org.mybatis.dynamic.sql.insert.render.MultiRowInsertStatementProvider;
import org.mybatis.dynamic.sql.select.CountDSLCompleter;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.UpdateDSL;
import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
import org.mybatis.dynamic.sql.update.UpdateModel;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;

@Mapper
public interface CourseInfoMapper {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.942+08:00", comments="Source Table: course_info")
    BasicColumn[] selectList = BasicColumn.columnList(id, courseId, courseHour, courseUrl, courseName, createTime, updateTime);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source Table: course_info")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    long count(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source Table: course_info")
    @DeleteProvider(type=SqlProviderAdapter.class, method="delete")
    int delete(DeleteStatementProvider deleteStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source Table: course_info")
    @InsertProvider(type=SqlProviderAdapter.class, method="insert")
    int insert(InsertStatementProvider<CourseInfo> insertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source Table: course_info")
    @InsertProvider(type=SqlProviderAdapter.class, method="insertMultiple")
    int insertMultiple(MultiRowInsertStatementProvider<CourseInfo> multipleInsertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source Table: course_info")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @ResultMap("CourseInfoResult")
    Optional<CourseInfo> selectOne(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source Table: course_info")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="CourseInfoResult", value = {
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="course_id", property="courseId", jdbcType=JdbcType.VARCHAR),
        @Result(column="course_hour", property="courseHour", jdbcType=JdbcType.INTEGER),
        @Result(column="course_url", property="courseUrl", jdbcType=JdbcType.VARCHAR),
        @Result(column="course_name", property="courseName", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<CourseInfo> selectMany(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source Table: course_info")
    @UpdateProvider(type=SqlProviderAdapter.class, method="update")
    int update(UpdateStatementProvider updateStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source Table: course_info")
    default long count(CountDSLCompleter completer) {
        return MyBatis3Utils.countFrom(this::count, courseInfo, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source Table: course_info")
    default int delete(DeleteDSLCompleter completer) {
        return MyBatis3Utils.deleteFrom(this::delete, courseInfo, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source Table: course_info")
    default int deleteByPrimaryKey(String id_) {
        return delete(c -> 
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source Table: course_info")
    default int insert(CourseInfo record) {
        return MyBatis3Utils.insert(this::insert, record, courseInfo, c ->
            c.map(id).toProperty("id")
            .map(courseId).toProperty("courseId")
            .map(courseHour).toProperty("courseHour")
            .map(courseUrl).toProperty("courseUrl")
            .map(courseName).toProperty("courseName")
            .map(createTime).toProperty("createTime")
            .map(updateTime).toProperty("updateTime")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source Table: course_info")
    default int insertMultiple(Collection<CourseInfo> records) {
        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, courseInfo, c ->
            c.map(id).toProperty("id")
            .map(courseId).toProperty("courseId")
            .map(courseHour).toProperty("courseHour")
            .map(courseUrl).toProperty("courseUrl")
            .map(courseName).toProperty("courseName")
            .map(createTime).toProperty("createTime")
            .map(updateTime).toProperty("updateTime")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.942+08:00", comments="Source Table: course_info")
    default int insertSelective(CourseInfo record) {
        return MyBatis3Utils.insert(this::insert, record, courseInfo, c ->
            c.map(id).toPropertyWhenPresent("id", record::getId)
            .map(courseId).toPropertyWhenPresent("courseId", record::getCourseId)
            .map(courseHour).toPropertyWhenPresent("courseHour", record::getCourseHour)
            .map(courseUrl).toPropertyWhenPresent("courseUrl", record::getCourseUrl)
            .map(courseName).toPropertyWhenPresent("courseName", record::getCourseName)
            .map(createTime).toPropertyWhenPresent("createTime", record::getCreateTime)
            .map(updateTime).toPropertyWhenPresent("updateTime", record::getUpdateTime)
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.942+08:00", comments="Source Table: course_info")
    default Optional<CourseInfo> selectOne(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectOne(this::selectOne, selectList, courseInfo, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.942+08:00", comments="Source Table: course_info")
    default List<CourseInfo> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, courseInfo, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.942+08:00", comments="Source Table: course_info")
    default List<CourseInfo> selectDistinct(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, courseInfo, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.942+08:00", comments="Source Table: course_info")
    default Optional<CourseInfo> selectByPrimaryKey(String id_) {
        return selectOne(c ->
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.942+08:00", comments="Source Table: course_info")
    default int update(UpdateDSLCompleter completer) {
        return MyBatis3Utils.update(this::update, courseInfo, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.942+08:00", comments="Source Table: course_info")
    static UpdateDSL<UpdateModel> updateAllColumns(CourseInfo record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalTo(record::getId)
                .set(courseId).equalTo(record::getCourseId)
                .set(courseHour).equalTo(record::getCourseHour)
                .set(courseUrl).equalTo(record::getCourseUrl)
                .set(courseName).equalTo(record::getCourseName)
                .set(createTime).equalTo(record::getCreateTime)
                .set(updateTime).equalTo(record::getUpdateTime);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.942+08:00", comments="Source Table: course_info")
    static UpdateDSL<UpdateModel> updateSelectiveColumns(CourseInfo record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalToWhenPresent(record::getId)
                .set(courseId).equalToWhenPresent(record::getCourseId)
                .set(courseHour).equalToWhenPresent(record::getCourseHour)
                .set(courseUrl).equalToWhenPresent(record::getCourseUrl)
                .set(courseName).equalToWhenPresent(record::getCourseName)
                .set(createTime).equalToWhenPresent(record::getCreateTime)
                .set(updateTime).equalToWhenPresent(record::getUpdateTime);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.942+08:00", comments="Source Table: course_info")
    default int updateByPrimaryKey(CourseInfo record) {
        return update(c ->
            c.set(courseId).equalTo(record::getCourseId)
            .set(courseHour).equalTo(record::getCourseHour)
            .set(courseUrl).equalTo(record::getCourseUrl)
            .set(courseName).equalTo(record::getCourseName)
            .set(createTime).equalTo(record::getCreateTime)
            .set(updateTime).equalTo(record::getUpdateTime)
            .where(id, isEqualTo(record::getId))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.942+08:00", comments="Source Table: course_info")
    default int updateByPrimaryKeySelective(CourseInfo record) {
        return update(c ->
            c.set(courseId).equalToWhenPresent(record::getCourseId)
            .set(courseHour).equalToWhenPresent(record::getCourseHour)
            .set(courseUrl).equalToWhenPresent(record::getCourseUrl)
            .set(courseName).equalToWhenPresent(record::getCourseName)
            .set(createTime).equalToWhenPresent(record::getCreateTime)
            .set(updateTime).equalToWhenPresent(record::getUpdateTime)
            .where(id, isEqualTo(record::getId))
        );
    }
}