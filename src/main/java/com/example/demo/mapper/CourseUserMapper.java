package com.example.demo.mapper;

import static com.example.demo.mapper.CourseUserDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.*;

import com.example.demo.model.CourseUser;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.annotation.Generated;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
import org.mybatis.dynamic.sql.insert.render.MultiRowInsertStatementProvider;
import org.mybatis.dynamic.sql.select.CountDSLCompleter;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.UpdateDSL;
import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
import org.mybatis.dynamic.sql.update.UpdateModel;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;

@Mapper
public interface CourseUserMapper {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    BasicColumn[] selectList = BasicColumn.columnList(id, courseId, userId, createTime, updateTime, evaluate, note);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.945+08:00", comments="Source Table: course_user")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    long count(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    @DeleteProvider(type=SqlProviderAdapter.class, method="delete")
    int delete(DeleteStatementProvider deleteStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    @InsertProvider(type=SqlProviderAdapter.class, method="insert")
    int insert(InsertStatementProvider<CourseUser> insertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    @InsertProvider(type=SqlProviderAdapter.class, method="insertMultiple")
    int insertMultiple(MultiRowInsertStatementProvider<CourseUser> multipleInsertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @ResultMap("CourseUserResult")
    Optional<CourseUser> selectOne(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="CourseUserResult", value = {
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="course_id", property="courseId", jdbcType=JdbcType.VARCHAR),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="evaluate", property="evaluate", jdbcType=JdbcType.VARCHAR),
        @Result(column="note", property="note", jdbcType=JdbcType.VARCHAR)
    })
    List<CourseUser> selectMany(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    @UpdateProvider(type=SqlProviderAdapter.class, method="update")
    int update(UpdateStatementProvider updateStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    default long count(CountDSLCompleter completer) {
        return MyBatis3Utils.countFrom(this::count, courseUser, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    default int delete(DeleteDSLCompleter completer) {
        return MyBatis3Utils.deleteFrom(this::delete, courseUser, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    default int deleteByPrimaryKey(String id_) {
        return delete(c -> 
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    default int insert(CourseUser record) {
        return MyBatis3Utils.insert(this::insert, record, courseUser, c ->
            c.map(id).toProperty("id")
            .map(courseId).toProperty("courseId")
            .map(userId).toProperty("userId")
            .map(createTime).toProperty("createTime")
            .map(updateTime).toProperty("updateTime")
            .map(evaluate).toProperty("evaluate")
            .map(note).toProperty("note")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    default int insertMultiple(Collection<CourseUser> records) {
        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, courseUser, c ->
            c.map(id).toProperty("id")
            .map(courseId).toProperty("courseId")
            .map(userId).toProperty("userId")
            .map(createTime).toProperty("createTime")
            .map(updateTime).toProperty("updateTime")
            .map(evaluate).toProperty("evaluate")
            .map(note).toProperty("note")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    default int insertSelective(CourseUser record) {
        return MyBatis3Utils.insert(this::insert, record, courseUser, c ->
            c.map(id).toPropertyWhenPresent("id", record::getId)
            .map(courseId).toPropertyWhenPresent("courseId", record::getCourseId)
            .map(userId).toPropertyWhenPresent("userId", record::getUserId)
            .map(createTime).toPropertyWhenPresent("createTime", record::getCreateTime)
            .map(updateTime).toPropertyWhenPresent("updateTime", record::getUpdateTime)
            .map(evaluate).toPropertyWhenPresent("evaluate", record::getEvaluate)
            .map(note).toPropertyWhenPresent("note", record::getNote)
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    default Optional<CourseUser> selectOne(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectOne(this::selectOne, selectList, courseUser, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    default List<CourseUser> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, courseUser, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    default List<CourseUser> selectDistinct(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, courseUser, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    default Optional<CourseUser> selectByPrimaryKey(String id_) {
        return selectOne(c ->
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    default int update(UpdateDSLCompleter completer) {
        return MyBatis3Utils.update(this::update, courseUser, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    static UpdateDSL<UpdateModel> updateAllColumns(CourseUser record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalTo(record::getId)
                .set(courseId).equalTo(record::getCourseId)
                .set(userId).equalTo(record::getUserId)
                .set(createTime).equalTo(record::getCreateTime)
                .set(updateTime).equalTo(record::getUpdateTime)
                .set(evaluate).equalTo(record::getEvaluate)
                .set(note).equalTo(record::getNote);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    static UpdateDSL<UpdateModel> updateSelectiveColumns(CourseUser record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalToWhenPresent(record::getId)
                .set(courseId).equalToWhenPresent(record::getCourseId)
                .set(userId).equalToWhenPresent(record::getUserId)
                .set(createTime).equalToWhenPresent(record::getCreateTime)
                .set(updateTime).equalToWhenPresent(record::getUpdateTime)
                .set(evaluate).equalToWhenPresent(record::getEvaluate)
                .set(note).equalToWhenPresent(record::getNote);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.946+08:00", comments="Source Table: course_user")
    default int updateByPrimaryKey(CourseUser record) {
        return update(c ->
            c.set(courseId).equalTo(record::getCourseId)
            .set(userId).equalTo(record::getUserId)
            .set(createTime).equalTo(record::getCreateTime)
            .set(updateTime).equalTo(record::getUpdateTime)
            .set(evaluate).equalTo(record::getEvaluate)
            .set(note).equalTo(record::getNote)
            .where(id, isEqualTo(record::getId))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.947+08:00", comments="Source Table: course_user")
    default int updateByPrimaryKeySelective(CourseUser record) {
        return update(c ->
            c.set(courseId).equalToWhenPresent(record::getCourseId)
            .set(userId).equalToWhenPresent(record::getUserId)
            .set(createTime).equalToWhenPresent(record::getCreateTime)
            .set(updateTime).equalToWhenPresent(record::getUpdateTime)
            .set(evaluate).equalToWhenPresent(record::getEvaluate)
            .set(note).equalToWhenPresent(record::getNote)
            .where(id, isEqualTo(record::getId))
        );
    }
}