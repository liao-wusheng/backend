package com.example.demo.mapper;

import java.sql.JDBCType;
import java.util.Date;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class CourseUserDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.945+08:00", comments="Source Table: course_user")
    public static final CourseUser courseUser = new CourseUser();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.945+08:00", comments="Source field: course_user.id")
    public static final SqlColumn<String> id = courseUser.id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.945+08:00", comments="Source field: course_user.course_id")
    public static final SqlColumn<String> courseId = courseUser.courseId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.945+08:00", comments="Source field: course_user.user_id")
    public static final SqlColumn<String> userId = courseUser.userId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.945+08:00", comments="Source field: course_user.create_time")
    public static final SqlColumn<Date> createTime = courseUser.createTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.945+08:00", comments="Source field: course_user.update_time")
    public static final SqlColumn<Date> updateTime = courseUser.updateTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.945+08:00", comments="Source field: course_user.evaluate")
    public static final SqlColumn<String> evaluate = courseUser.evaluate;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.945+08:00", comments="Source field: course_user.note")
    public static final SqlColumn<String> note = courseUser.note;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.945+08:00", comments="Source Table: course_user")
    public static final class CourseUser extends SqlTable {
        public final SqlColumn<String> id = column("id", JDBCType.VARCHAR);

        public final SqlColumn<String> courseId = column("course_id", JDBCType.VARCHAR);

        public final SqlColumn<String> userId = column("user_id", JDBCType.VARCHAR);

        public final SqlColumn<Date> createTime = column("create_time", JDBCType.TIMESTAMP);

        public final SqlColumn<Date> updateTime = column("update_time", JDBCType.TIMESTAMP);

        public final SqlColumn<String> evaluate = column("evaluate", JDBCType.VARCHAR);

        public final SqlColumn<String> note = column("note", JDBCType.VARCHAR);

        public CourseUser() {
            super("course_user");
        }
    }
}