package com.example.demo.mapper;

import java.sql.JDBCType;
import java.util.Date;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class RoleDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.943+08:00", comments="Source Table: role")
    public static final Role role = new Role();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.943+08:00", comments="Source field: role.id")
    public static final SqlColumn<Long> id = role.id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.943+08:00", comments="Source field: role.name")
    public static final SqlColumn<String> name = role.name;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.943+08:00", comments="Source field: role.description")
    public static final SqlColumn<String> description = role.description;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.943+08:00", comments="Source field: role.create_time")
    public static final SqlColumn<Date> createTime = role.createTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.943+08:00", comments="Source field: role.update_time")
    public static final SqlColumn<Date> updateTime = role.updateTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.943+08:00", comments="Source Table: role")
    public static final class Role extends SqlTable {
        public final SqlColumn<Long> id = column("id", JDBCType.BIGINT);

        public final SqlColumn<String> name = column("name", JDBCType.VARCHAR);

        public final SqlColumn<String> description = column("description", JDBCType.VARCHAR);

        public final SqlColumn<Date> createTime = column("create_time", JDBCType.TIMESTAMP);

        public final SqlColumn<Date> updateTime = column("update_time", JDBCType.TIMESTAMP);

        public Role() {
            super("role");
        }
    }
}