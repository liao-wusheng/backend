package com.example.demo.mapper;

import java.sql.JDBCType;
import java.util.Date;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class UserRoleDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source Table: user_role")
    public static final UserRole userRole = new UserRole();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.userID")
    public static final SqlColumn<String> userid = userRole.userid;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.roleID")
    public static final SqlColumn<Long> roleid = userRole.roleid;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.state")
    public static final SqlColumn<String> state = userRole.state;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source field: user_role.create_time")
    public static final SqlColumn<Date> createTime = userRole.createTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.951+08:00", comments="Source field: user_role.update_time")
    public static final SqlColumn<Date> updateTime = userRole.updateTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.95+08:00", comments="Source Table: user_role")
    public static final class UserRole extends SqlTable {
        public final SqlColumn<String> userid = column("userID", JDBCType.VARCHAR);

        public final SqlColumn<Long> roleid = column("roleID", JDBCType.BIGINT);

        public final SqlColumn<String> state = column("state", JDBCType.CHAR);

        public final SqlColumn<Date> createTime = column("create_time", JDBCType.TIMESTAMP);

        public final SqlColumn<Date> updateTime = column("update_time", JDBCType.TIMESTAMP);

        public UserRole() {
            super("user_role");
        }
    }
}