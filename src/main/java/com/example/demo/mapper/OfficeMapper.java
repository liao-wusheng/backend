package com.example.demo.mapper;

import static com.example.demo.mapper.OfficeDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.*;

import com.example.demo.model.Office;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.annotation.Generated;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
import org.mybatis.dynamic.sql.insert.render.MultiRowInsertStatementProvider;
import org.mybatis.dynamic.sql.select.CountDSLCompleter;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.UpdateDSL;
import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
import org.mybatis.dynamic.sql.update.UpdateModel;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;

@Mapper
public interface OfficeMapper {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.287+08:00", comments="Source Table: office")
    BasicColumn[] selectList = BasicColumn.columnList(id, name, teacher, createTime, updateTime, imgUrl, description);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.28+08:00", comments="Source Table: office")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    long count(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.28+08:00", comments="Source Table: office")
    @DeleteProvider(type=SqlProviderAdapter.class, method="delete")
    int delete(DeleteStatementProvider deleteStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.281+08:00", comments="Source Table: office")
    @InsertProvider(type=SqlProviderAdapter.class, method="insert")
    int insert(InsertStatementProvider<Office> insertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.281+08:00", comments="Source Table: office")
    @InsertProvider(type=SqlProviderAdapter.class, method="insertMultiple")
    int insertMultiple(MultiRowInsertStatementProvider<Office> multipleInsertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.282+08:00", comments="Source Table: office")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @ResultMap("OfficeResult")
    Optional<Office> selectOne(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.282+08:00", comments="Source Table: office")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="OfficeResult", value = {
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="teacher", property="teacher", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="img_url", property="imgUrl", jdbcType=JdbcType.VARCHAR),
        @Result(column="description", property="description", jdbcType=JdbcType.LONGVARCHAR)
    })
    List<Office> selectMany(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.283+08:00", comments="Source Table: office")
    @UpdateProvider(type=SqlProviderAdapter.class, method="update")
    int update(UpdateStatementProvider updateStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.283+08:00", comments="Source Table: office")
    default long count(CountDSLCompleter completer) {
        return MyBatis3Utils.countFrom(this::count, office, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.284+08:00", comments="Source Table: office")
    default int delete(DeleteDSLCompleter completer) {
        return MyBatis3Utils.deleteFrom(this::delete, office, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.284+08:00", comments="Source Table: office")
    default int deleteByPrimaryKey(String id_) {
        return delete(c -> 
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.284+08:00", comments="Source Table: office")
    default int insert(Office record) {
        return MyBatis3Utils.insert(this::insert, record, office, c ->
            c.map(id).toProperty("id")
            .map(name).toProperty("name")
            .map(teacher).toProperty("teacher")
            .map(createTime).toProperty("createTime")
            .map(updateTime).toProperty("updateTime")
            .map(imgUrl).toProperty("imgUrl")
            .map(description).toProperty("description")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.285+08:00", comments="Source Table: office")
    default int insertMultiple(Collection<Office> records) {
        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, office, c ->
            c.map(id).toProperty("id")
            .map(name).toProperty("name")
            .map(teacher).toProperty("teacher")
            .map(createTime).toProperty("createTime")
            .map(updateTime).toProperty("updateTime")
            .map(imgUrl).toProperty("imgUrl")
            .map(description).toProperty("description")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.286+08:00", comments="Source Table: office")
    default int insertSelective(Office record) {
        return MyBatis3Utils.insert(this::insert, record, office, c ->
            c.map(id).toPropertyWhenPresent("id", record::getId)
            .map(name).toPropertyWhenPresent("name", record::getName)
            .map(teacher).toPropertyWhenPresent("teacher", record::getTeacher)
            .map(createTime).toPropertyWhenPresent("createTime", record::getCreateTime)
            .map(updateTime).toPropertyWhenPresent("updateTime", record::getUpdateTime)
            .map(imgUrl).toPropertyWhenPresent("imgUrl", record::getImgUrl)
            .map(description).toPropertyWhenPresent("description", record::getDescription)
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.288+08:00", comments="Source Table: office")
    default Optional<Office> selectOne(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectOne(this::selectOne, selectList, office, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.288+08:00", comments="Source Table: office")
    default List<Office> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, office, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.288+08:00", comments="Source Table: office")
    default List<Office> selectDistinct(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, office, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.289+08:00", comments="Source Table: office")
    default Optional<Office> selectByPrimaryKey(String id_) {
        return selectOne(c ->
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.289+08:00", comments="Source Table: office")
    default int update(UpdateDSLCompleter completer) {
        return MyBatis3Utils.update(this::update, office, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.289+08:00", comments="Source Table: office")
    static UpdateDSL<UpdateModel> updateAllColumns(Office record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalTo(record::getId)
                .set(name).equalTo(record::getName)
                .set(teacher).equalTo(record::getTeacher)
                .set(createTime).equalTo(record::getCreateTime)
                .set(updateTime).equalTo(record::getUpdateTime)
                .set(imgUrl).equalTo(record::getImgUrl)
                .set(description).equalTo(record::getDescription);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.29+08:00", comments="Source Table: office")
    static UpdateDSL<UpdateModel> updateSelectiveColumns(Office record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalToWhenPresent(record::getId)
                .set(name).equalToWhenPresent(record::getName)
                .set(teacher).equalToWhenPresent(record::getTeacher)
                .set(createTime).equalToWhenPresent(record::getCreateTime)
                .set(updateTime).equalToWhenPresent(record::getUpdateTime)
                .set(imgUrl).equalToWhenPresent(record::getImgUrl)
                .set(description).equalToWhenPresent(record::getDescription);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.29+08:00", comments="Source Table: office")
    default int updateByPrimaryKey(Office record) {
        return update(c ->
            c.set(name).equalTo(record::getName)
            .set(teacher).equalTo(record::getTeacher)
            .set(createTime).equalTo(record::getCreateTime)
            .set(updateTime).equalTo(record::getUpdateTime)
            .set(imgUrl).equalTo(record::getImgUrl)
            .set(description).equalTo(record::getDescription)
            .where(id, isEqualTo(record::getId))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-26T00:46:16.291+08:00", comments="Source Table: office")
    default int updateByPrimaryKeySelective(Office record) {
        return update(c ->
            c.set(name).equalToWhenPresent(record::getName)
            .set(teacher).equalToWhenPresent(record::getTeacher)
            .set(createTime).equalToWhenPresent(record::getCreateTime)
            .set(updateTime).equalToWhenPresent(record::getUpdateTime)
            .set(imgUrl).equalToWhenPresent(record::getImgUrl)
            .set(description).equalToWhenPresent(record::getDescription)
            .where(id, isEqualTo(record::getId))
        );
    }
}