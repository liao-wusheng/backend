package com.example.demo.mapper;

import java.sql.JDBCType;
import java.util.Date;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class CourseInfoDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source Table: course_info")
    public static final CourseInfo courseInfo = new CourseInfo();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source field: course_info.id")
    public static final SqlColumn<String> id = courseInfo.id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source field: course_info.course_id")
    public static final SqlColumn<String> courseId = courseInfo.courseId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source field: course_info.course_hour")
    public static final SqlColumn<Integer> courseHour = courseInfo.courseHour;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source field: course_info.course_url")
    public static final SqlColumn<String> courseUrl = courseInfo.courseUrl;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source field: course_info.course_name")
    public static final SqlColumn<String> courseName = courseInfo.courseName;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source field: course_info.create_time")
    public static final SqlColumn<Date> createTime = courseInfo.createTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source field: course_info.update_time")
    public static final SqlColumn<Date> updateTime = courseInfo.updateTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.941+08:00", comments="Source Table: course_info")
    public static final class CourseInfo extends SqlTable {
        public final SqlColumn<String> id = column("id", JDBCType.VARCHAR);

        public final SqlColumn<String> courseId = column("course_id", JDBCType.VARCHAR);

        public final SqlColumn<Integer> courseHour = column("course_hour", JDBCType.INTEGER);

        public final SqlColumn<String> courseUrl = column("course_url", JDBCType.VARCHAR);

        public final SqlColumn<String> courseName = column("course_name", JDBCType.VARCHAR);

        public final SqlColumn<Date> createTime = column("create_time", JDBCType.TIMESTAMP);

        public final SqlColumn<Date> updateTime = column("update_time", JDBCType.TIMESTAMP);

        public CourseInfo() {
            super("course_info");
        }
    }
}