package com.example.demo.mapper;

import java.sql.JDBCType;
import java.util.Date;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class CourseDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.326+08:00", comments="Source Table: course")
    public static final Course course = new Course();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.326+08:00", comments="Source field: course.id")
    public static final SqlColumn<String> id = course.id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.326+08:00", comments="Source field: course.image_url")
    public static final SqlColumn<String> imageUrl = course.imageUrl;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.326+08:00", comments="Source field: course.name")
    public static final SqlColumn<String> name = course.name;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.326+08:00", comments="Source field: course.user_id")
    public static final SqlColumn<String> userId = course.userId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.326+08:00", comments="Source field: course.class_hour")
    public static final SqlColumn<Integer> classHour = course.classHour;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.326+08:00", comments="Source field: course.create_time")
    public static final SqlColumn<Date> createTime = course.createTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.326+08:00", comments="Source field: course.update_time")
    public static final SqlColumn<Date> updateTime = course.updateTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.326+08:00", comments="Source field: course.type")
    public static final SqlColumn<String> type = course.type;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.326+08:00", comments="Source field: course.state")
    public static final SqlColumn<Integer> state = course.state;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.326+08:00", comments="Source field: course.description")
    public static final SqlColumn<String> description = course.description;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-25T00:44:07.326+08:00", comments="Source Table: course")
    public static final class Course extends SqlTable {
        public final SqlColumn<String> id = column("id", JDBCType.VARCHAR);

        public final SqlColumn<String> imageUrl = column("image_url", JDBCType.VARCHAR);

        public final SqlColumn<String> name = column("name", JDBCType.VARCHAR);

        public final SqlColumn<String> userId = column("user_id", JDBCType.VARCHAR);

        public final SqlColumn<Integer> classHour = column("class_hour", JDBCType.INTEGER);

        public final SqlColumn<Date> createTime = column("create_time", JDBCType.TIMESTAMP);

        public final SqlColumn<Date> updateTime = column("update_time", JDBCType.TIMESTAMP);

        public final SqlColumn<String> type = column("type", JDBCType.CHAR);

        public final SqlColumn<Integer> state = column("state", JDBCType.INTEGER);

        public final SqlColumn<String> description = column("description", JDBCType.LONGVARCHAR);

        public Course() {
            super("course");
        }
    }
}