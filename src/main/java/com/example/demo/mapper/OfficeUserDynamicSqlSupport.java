package com.example.demo.mapper;

import java.sql.JDBCType;
import java.util.Date;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class OfficeUserDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.935+08:00", comments="Source Table: office_user")
    public static final OfficeUser officeUser = new OfficeUser();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.935+08:00", comments="Source field: office_user.id")
    public static final SqlColumn<String> id = officeUser.id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.935+08:00", comments="Source field: office_user.user_id")
    public static final SqlColumn<String> userId = officeUser.userId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.935+08:00", comments="Source field: office_user.class_id")
    public static final SqlColumn<String> classId = officeUser.classId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.935+08:00", comments="Source field: office_user.create_time")
    public static final SqlColumn<Date> createTime = officeUser.createTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.935+08:00", comments="Source field: office_user.update_time")
    public static final SqlColumn<Date> updateTime = officeUser.updateTime;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2022-10-24T23:17:30.935+08:00", comments="Source Table: office_user")
    public static final class OfficeUser extends SqlTable {
        public final SqlColumn<String> id = column("id", JDBCType.VARCHAR);

        public final SqlColumn<String> userId = column("user_id", JDBCType.VARCHAR);

        public final SqlColumn<String> classId = column("class_id", JDBCType.VARCHAR);

        public final SqlColumn<Date> createTime = column("create_time", JDBCType.TIMESTAMP);

        public final SqlColumn<Date> updateTime = column("update_time", JDBCType.TIMESTAMP);

        public OfficeUser() {
            super("office_user");
        }
    }
}