create database if not exists  tonghongyun;

drop table if exists user,course,course_info,course_infop_user,class,class_user,course_user,role,user_role;

use tonghongyun;

create table user(
    id  varchar (64)    primary key                     comment 'ID',
    head_picture_url varchar(512) null                  comment '头像地址',
    nick_name varchar(30) null                          comment '用户昵称',
    password varchar(20) null                           comment '用户密码',
    state char(1) null                                  comment '用户状态',
    open_id varchar (512) null                          comment '微信id',
    create_time datetime null                           comment '创建时间',
    update_time datetime null                           comment '修改时间',
    department bigint null                              comment '学院专业',
    e_mail varchar(50) null                             comment '用户邮箱',
    stu_num varchar (15) null                           comment '学号',
    phone varchar(15) null                              comment '用户手机'
) comment '用户表';


create table role(
    id bigint primary key auto_increment                comment '角色ID',
    name varchar(20) null                               comment '姓名',
    description varchar(512) null                       comment '描述',
    create_time datetime null                           comment '创建时间',
    update_time datetime null                           comment '修改时间'
)comment '系统角色表';

create table user_role(
    userID   varchar (64)                               comment '用户ID',
    roleID   bigint                                     comment '角色ID',
    state char(1) null                                  comment '用户系统中角色的状态',
    create_time datetime null                           comment '创建时间',
    update_time datetime null                           comment '修改时间',
    primary key (userID,roleID)
)comment '用户角色表';

create table course(
    id varchar (64) primary key                         comment '课程id',
    image_url varchar (512) null                        comment '课程封面图案url',
    name varchar (64) null                              comment '课程名称',
    user_id varchar (64) null                           comment '讲师id',
    description longtext null                           comment '课程描述',
    class_hour int  null                                comment '课时',
    state  int null                                     comment '课程状态',
    type char(1) null                                   comment '课程类别',
    create_time datetime null                           comment '创建时间',
    update_time datetime null                           comment '修改时间'
)comment '课程表';

create table course_info(
    id varchar (64) primary key                         comment '课程学时id',
    course_id varchar (64) null                         comment '课程id',
    course_hour int null                                comment '课程第几学时',
    course_url varchar (512) null                       comment '课时具体url',
    course_name varchar (128) null                      comment '课时具体名称',
    create_time datetime null                           comment '创建时间',
    update_time datetime null                           comment '修改时间'
)comment '课程具体学时信息表';

create table course_user(
    id varchar (64) primary key                         comment '课程成员信息表',
    course_id varchar (64) null                         comment '课程id',
    user_id varchar (64) null                           comment '课程成员id',
    note varchar (1024) null                            comment '课程学习笔记',
    evaluate varchar (1024) null                        comment '课程评价',
    create_time datetime null                           comment '创建时间',
    update_time datetime null                           comment '修改时间'
)comment '课程成员表';

create table course_info_user(
    id varchar (64) primary key                         comment 'id',
    course_info_id varchar (64) null                    comment '课程具体学时id',
    user_id varchar (64) null                           comment '用户id',
    state int null                                      comment '课时学习状态',
    note varchar (1024) null                            comment '学时学习笔记',
    create_time datetime null                           comment '创建时间',
    update_time datetime null                           comment '修改时间'
)comment '课程学时学习表';

create table office(
    id varchar (64) primary key                         comment '班级id',
    name varchar (64) null                              comment '班级名称',
    teacher varchar (64) null                           comment '老师id',
    state int null                                      comment '班级状态',
    description longtext null                           comment '班级描述',
    create_time datetime null                           comment '创建时间',
    update_time datetime null                           comment '修改时间'
)comment '班级表';

create table office_user(
    id varchar (64) primary key                         comment 'id',
    user_id varchar (64) null                           comment '用户id',
    class_id varchar (64) null                          comment '班级id',
    create_time datetime null                           comment '创建时间',
    update_time datetime null                           comment '修改时间'
)comment '班级成员信息表';


insert into role values ('1','老师','系统管理员以及课程老师','2022-10-22 19:02:28','2022-10-22 19:02:28'),
                        ('2','学生','普通用户及课程学生','2022-10-22 19:02:28','2022-10-22 19:02:28');
